import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { Switch, Route } from 'react-router-dom';

import Header from '../header';
import Items from '../../containers/items';
import Modal from '../../widgets/modal';
import Button from '../../widgets/button';
import PopUp from '../../widgets/popUp';

import './Layout.scss';

class Layout extends Component {
  constructor(props) {
    super(props);
    props.getTypes();

    props.history.listen((routeData) => {
      this.setLayout(routeData);
    });
  }

  setLayout = (data) => {
    if (data.state) {
      const { material } = data.state;
      this.props.updateLayout({
        theme: material,
        popUpOpen: false,
        filters: { ...data.state }
      });
    } else {
      this.props.resetLayout();
    }
  }

  openPopUp = (menu) => {
    const { popUpOpen } = this.props.layout;
    this.props.updateLayout({
      popUpOpen: !popUpOpen,
      section: menu
    });
  }

  popUp = () => {
    const { section } = this.props.layout;
    let options;

    switch (section) {
    case 'filter-items':
    case 'add-item':
      options = ['brick', 'block', 'rebar', 'steel', 'timber', 'other'];
      break;
    default:
      options = [];
      break;
    }
    return (
      <PopUp type={section} options={options} />
    );
  }

  render = () => {
    const { popUpOpen, theme, filters } = this.props.layout;

    return (
      <div className={`app-layout ${theme}`}>
        {popUpOpen ? this.popUp() : ''}
        <Header theme={theme} title="All Materials">
          <div className="actions">
            <Button
              dispatch={this.openPopUp}
              id="add-item"
              icon="fa fa-plus"
              type="icon"
              theme="dark"
            />
          </div>
        </Header>
        <Switch>
          <Route
            exact
            path="/items"
            render={props => (
              <Items
                {...props}
                filters={filters}
                setLayout={this.setLayout}
              />
            )}
          />
          <Route
            exact
            path="/items/:material"
            render={props => (
              <Items
                {...props}
                filters={filters}
                setLayout={this.setLayout}
              />
            )}
          />
          <Route
            exact
            path="/items/:material/add"
            render={props => (
              <Modal
                {...props}
                setLayout={this.setLayout}
                content="add-item"
                theme={theme}
              />
            )}
          />
          <Route
            exact
            path="/items/:material/order"
            render={props => (
              <Modal
                {...props}
                setLayout={this.setLayout}
                content="order-item"
                theme={theme}
              />
            )}
          />
        </Switch>
      </div>
    );
  }
}

export default Layout;

Layout.propTypes = {
  getTypes: PropTypes.func.isRequired,
  history: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.object,
    PropTypes.func
  ])).isRequired,
  layout: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
    PropTypes.number,
    PropTypes.bool
  ])).isRequired,
  updateLayout: PropTypes.func.isRequired,
  resetLayout: PropTypes.func.isRequired
};
