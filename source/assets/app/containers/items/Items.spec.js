import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';

import Items from './';
// Not sure there is any point to this spec...
// Would be good to test if the props passed matched
// the name of an array of expected props
describe('Items Container', () => {
  const mockStore = configureStore();
  let container;
  let store;

  beforeEach(() => {
    store = mockStore();

    container = shallow(<Items store={store} />);
  });

  describe('Container Exists', () => {
    it('Should render the Items Container', () => {
      expect(container.length).toEqual(1);
    });
  });
});
