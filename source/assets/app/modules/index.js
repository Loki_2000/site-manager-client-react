import {
  itemsReducer as items,
  itemIDsReducer as itemIDs,
  selectedItemID
} from './items/reducer';

import {
  typesReducer as types,
  typeIDsReducer as typeIDs
} from './types/reducer';

import formData from './formData/reducer';
import user from './user/reducer';
import layout from './layout/reducer';

import { getItemsEpic, addItemsEpic, orderItemEpic } from './items/epics';
import { sendLoginRequest, checkUserLogin } from './user/epics';
import getTypesEpic from './types/epics';

export const reducers = {
  formData,
  items,
  itemIDs,
  selectedItemID,
  layout,
  types,
  typeIDs,
  user
};

export const epics = [
  getTypesEpic,
  addItemsEpic,
  orderItemEpic,
  sendLoginRequest,
  checkUserLogin,
  getItemsEpic
];
