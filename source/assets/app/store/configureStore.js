import { createStore, compose, applyMiddleware, combineReducers } from 'redux';
import { routerMiddleware as createRouterMiddleware } from 'react-router-redux';
import { browserHistory } from 'react-router';

import { combineEpics, createEpicMiddleware } from 'redux-observable';

import { reducers, epics } from '../modules';

const routerMiddleware = createRouterMiddleware(browserHistory);
const initialState = {};

const reducer = combineReducers({ ...reducers });
const epic = combineEpics(...epics);

const epicMiddleware = createEpicMiddleware(epic);

const enchancer = compose(
  applyMiddleware(routerMiddleware, epicMiddleware),
  (process.env.NODE_ENV !== 'production' && window.devToolsExtension) ? window.devToolsExtension() : f => f
);

export default function configureStore() {
  const store = createStore(reducer, initialState, enchancer);
  return store;
}
