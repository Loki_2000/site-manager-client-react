import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { Link } from 'react-router-dom';

import Select from '../../../widgets/select';

class FilterItems extends Component {
  static propTypes = {
    filters: PropTypes.objectOf(PropTypes.string).isRequired,
    types: PropTypes.objectOf(PropTypes.objectOf(PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.bool,
      PropTypes.array
    ]))).isRequired,

    updateFormData: PropTypes.func.isRequired,
    updateLayout: PropTypes.func.isRequired
  }

  onUpdate = (field, value) => {
    const shouldBeInteger = field === 'length'
      || field === 'quantity';

    const data = {
      [field]: shouldBeInteger ? parseInt(value, 10) : value
    };

    this.props.updateFormData(data);

    const filters = {
      ...this.props.filters,
      ...data
    };

    this.props.updateLayout({ filters });
  }

  parseOptions = (field) => {
    const { types, filters } = this.props;

    let options = [];

    switch (field) {
    case 'material':
      options = Object.keys(types)
        .map(id => types[id][field]);

      break;
    case 'types':
    case 'diameters':
    case 'densities':
    case 'blocksizes':
      options = Object.keys(types)
        .filter(key => types[key].material === filters.material)
        .map(id => types[id][field]);

      break;
    case 'sections':
      if (filters.material !== 'steel') return [];
      if (!filters.type) return ['Please Choose Type'];

      options = Object.keys(types)
        .map(id => types[id])
        .filter(type => type.material === 'steel')
        .find(obj => obj.type === filters.type).sections;

      break;
    default:
    }
    // Ensure returned array is flattened with uniques values
    return [...new Set([].concat(...options))];
  }

  render = () => {
    const { filters } = this.props;

    const materials = ['brick', 'block', 'rebar', 'steel', 'timber', 'other'];

    const selected = filters.material || 'all';

    return (
      <div>
        <div className="select-material">
          <div
            id="all"
            className={selected === 'all' ? 'material selected' : 'material'}
          >
            <Link
              to={{
                pathname: '/items'
              }}
            >
              All
            </Link>
          </div>
          {
            materials.map(option => (
              <div
                key={option}
                id={option}
                className={option === selected ? 'material selected' : 'material'}
              >
                <Link
                  to={{
                    pathname: `/items/${option === 'all' ? '' : option}`,
                    state: { material: option }
                  }}
                >
                  {option}
                </Link>
              </div>
            ))
          }
        </div>
        <div className="filters">
          <Select
            display={['steel', 'brick', 'timber'].includes(filters.material)}
            field="type"
            label="Type"
            selected={filters.type}
            options={this.parseOptions('types')}
            handleChange={this.onUpdate}
            theme="light"
            size="sm"
            defaultPlaceholder="Filter"
          />
          <Select
            display={filters.material === 'steel'}
            field="section"
            label="Section"
            selected={filters.section}
            options={this.parseOptions('sections')}
            handleChange={this.onUpdate}
            theme="light"
            size="sm"
            defaultPlaceholder="Filter"
          />
          <Select
            display={filters.material === 'rebar'}
            field="diameter"
            label="Diameter"
            selected={filters.diameter}
            options={this.parseOptions('diameters')}
            handleChange={this.onUpdate}
            theme="light"
            size="sm"
            defaultPlaceholder="Filter"
          />
          <Select
            display={filters.material === 'block'}
            field="density"
            label="Densities"
            selected={filters.density}
            options={this.parseOptions('densities')}
            handleChange={this.onUpdate}
            theme="light"
            size="sm"
            defaultPlaceholder="Filter"
          />
        </div>
      </div>
    );
  }
}

export default FilterItems;
