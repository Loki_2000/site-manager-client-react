import React from 'react';
import PropTypes from 'prop-types';

import './Header.scss';

const Header = ({ title, children }) => (
  <div className="app-header">
    <div className="title">
      <h4>{title}</h4>
    </div>
    {children}
  </div>
);

export default Header;

Header.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
    PropTypes.element
  ])
};

Header.defaultProps = {
  children: []
};
