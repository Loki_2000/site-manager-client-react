import React from 'react';
import { PropTypes } from 'prop-types';

import * as displaySchema from './displaySchema';
import './Item.scss';

const Item = ({ item, id, selectItem }) => {
  const display = displaySchema[item.material];

  const subtitle = display.titles[2]
    ? `${item[display.titles[1]]} (${item[display.titles[2]]})`
    : item[display.titles[1]];

  const displayDetails = {
    title: item[display.titles[0]],
    subTitle: subtitle,
    details: display.details
      .reduce((obj, detail) => ({
        ...obj,
        [detail]: item[detail]
      }), {})
  };

  const displayDetail = (name, index) => {
    if (displayDetails.details[name]) {
      return (
        <div
          className="detail"
          key={`${id}_${index}`}
        >
          <b>{name}:</b> {displayDetails.details[name]}
        </div>
      );
    }
    return ('');
  };

  return (
    <div
      role="button"
      tabIndex="0"
      onClick={(() => selectItem(id, item.material))}
      onKeyDown={(() => {})}
      className="item"
    >
      <div className="item-symbol">
        <img src={`/assets/images/${item.material}.png`} alt="steel" />
      </div>
      <div className="item-info">
        <div className="item-section">
          <span className="item-title">{displayDetails.title}</span>
        </div>
        <div className="item-section">
          <span className="sub-title">{displayDetails.subTitle}</span>
        </div>
        <div className="item-section">
          {
            Object.keys(displayDetails.details)
              .map((key, index) => displayDetail(key, index))
          }
        </div>
      </div>
    </div>
  );
};

export default Item;

Item.propTypes = {
  selectItem: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  item: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.bool
  ])).isRequired
};
