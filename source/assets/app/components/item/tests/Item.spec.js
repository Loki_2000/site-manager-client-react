import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Item from '../';

describe('Item Component', () => {
  let component;
  let tree;

  const props = {
    id: '5a0d7369fa457a1d6e762558',
    item: {
      _id: '5a0d7369fa457a1d6e762558',
      material: 'steel',
      type: 'Equal Angles',
      section: '356x46x634',
      grade: 'S275',
      length: 12,
      quantity: 9
    },
    selectItem: jest.fn()
  };

  beforeEach(() => {
    component = shallow(<Item {...props} />);
    tree = toJson(component);
  });

  describe('Component Exists', () => {
    it('Should render the Item Component', () => {
      expect(component.length).toEqual(1);
    });
  });

  describe('Click Select Item', () => {
    it('should call selectItem with expected params', () => {
      component.find('div.item').simulate('click');
      expect(props.selectItem).toHaveBeenCalledWith(props.id, props.item.material);
    });
  });

  describe('Snapshot Match', () => {
    it('Should match previous snapshot', () => {
      expect(tree).toMatchSnapshot();
    });
  });
});
