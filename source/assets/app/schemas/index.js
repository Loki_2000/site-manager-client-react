import { schema } from 'normalizr';

export const item = new schema.Entity('items', {}, { idAttribute: '_id' });
export const itemSchema = { items: new schema.Array(item) };

export const type = new schema.Entity('types', {}, { idAttribute: '_id' });
export const typeSchema = { types: new schema.Array(type) };
