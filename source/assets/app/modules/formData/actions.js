export const UPDATE_FORM_DATA = 'formData/UPDATE_FORM_DATA';
export const RESET_FORM_DATA = 'formData/RESET_FORM_DATA';
export const SET_DEFAULT_DATA = 'formData/SET_DEFAULT_DATA';

export const updateFormData = formData => ({
  type: UPDATE_FORM_DATA,
  payload: { formData }
});

export const resetFormData = defaultData => ({
  type: RESET_FORM_DATA,
  payload: defaultData
});

export const setDefaultData = id => ({
  type: SET_DEFAULT_DATA,
  payload: id
});
