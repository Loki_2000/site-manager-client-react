import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Items from '../';

describe('Items Component', () => {
  let component;
  let tree;

  const props = {
    getItems: jest.fn(),
    selectItem: jest.fn(),
    setLayout: jest.fn(),
    openModal: jest.fn(),
    itemIDs: ['0987', '0980', '0981'],
    items: {
      '0987': {
        _id: '0987',
        material: 'steel',
        type: 'Equal Angles',
        section: '200x200'
      },
      '0980': {
        _id: '0980',
        material: 'steel',
        type: 'UC',
        section: '200x200'
      },
      '0981': {
        _id: '0981',
        material: 'rebar',
        diameter: 'H8'
      }
    },
    filters: {
      material: 'steel',
      type: 'UC'
    },
    history: {
      push: jest.fn()
    },
    match: {
      params: {
        material: 'steel'
      }
    }
  };

  beforeEach(() => {
    component = shallow(<Items {...props} />);
    tree = toJson(component);
  });

  describe('Component Exists', () => {
    it('Should render the Items Component', () => {
      expect(component.length).toEqual(1);
    });
  });

  describe('onSelectItem', () => {
    it('Should call selectItem() and openModal()', () => {
      const id = '09RE98E';
      const material = 'steel';

      component.instance().onSelectItem(id, material);

      expect(props.selectItem).toHaveBeenCalledWith(id);
      expect(props.history.push).toHaveBeenCalledWith({
        pathname: `/items/${material}/order`,
        state: { material }
      });
    });
  });

  describe('filterItems', () => {
    it('should return true if all filters are present on item', () => {
      let filtered;
      const filters = Object.values(props.filters);

      props.itemIDs.forEach((id) => {
        filtered = component.instance().filterItems(id);

        expect(filtered).toEqual(Object.values(props.items[id])
          .includes(filters[0]) && Object.values(props.items[id])
            .includes(filters[1]));
      });
    });
  });

  describe('Snapshot Match', () => {
    it('Should match previous snapshot', () => {
      expect(tree).toMatchSnapshot();
    });
  });
});
