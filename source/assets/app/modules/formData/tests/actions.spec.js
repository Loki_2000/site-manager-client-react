import { updateFormData, resetFormData } from '../actions';

describe('Items Actions', () => {
  const formData = {
    username: 'test@test.com'
  };

  describe('updateFormData', () => {
    it('should call updateFormData with expected action', () => {
      const expected = {
        type: 'formData/UPDATE_FORM_DATA',
        payload: { formData }
      };
      const action = updateFormData(formData);
      expect(action).toEqual(expected);
    });
  });

  describe('resetFormData', () => {
    it('should call resetFormData with expected action-type', () => {
      const expected = 'formData/RESET_FORM_DATA';

      const action = resetFormData();
      expect(action.type).toEqual(expected);
    });
  });
});
