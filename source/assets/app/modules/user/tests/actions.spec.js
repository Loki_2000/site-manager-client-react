import { requestLogin, userLoggedIn, checkLogin } from '../actions';

describe('User Actions', () => {

  describe('requestLogin', () => {
    const payload = {
      username: 'test@test.com',
      password: '1234'
    };

    it('should call requestLogin with expected details', () => {
      const expected = {
        type: 'user/REQUEST_LOGIN',
        payload
      };
      const action = requestLogin(payload);

      expect(action).toEqual(expected);
    });
  });

  describe('userLoggedIn', () => {
    const user = {
      username: 'test@test.com'
    };

    it('should call userLoggedIn with expected details', () => {
      const expected = {
        type: 'user/USER_LOGGED_IN',
        payload: { user }
      };
      const action = userLoggedIn(user);

      expect(action).toEqual(expected);
    });
  });

  describe('checkLogin', () => {
    const id = '0987';

    it('should call checkLogin with expected details', () => {
      const expected = {
        type: 'user/CHECK_LOGIN',
        id
      };
      const action = checkLogin(id);

      expect(action).toEqual(expected);
    });
  });
});
