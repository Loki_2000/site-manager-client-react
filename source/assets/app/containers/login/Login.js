import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Login from '../../components/login';
import { requestLogin } from '../../modules/user/actions';

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    requestLogin
  }, dispatch);
}

export default connect(null, mapDispatchToProps)(Login);
