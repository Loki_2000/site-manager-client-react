import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Login from '../';

describe('Items Component', () => {
  let component;
  let tree;

  const props = {
    data: {
      username: 'test@test.com',
      password: '1234'
    },
    update: jest.fn(),
    submit: jest.fn()
  };

  beforeEach(() => {
    component = shallow(<Login {...props} />);
    tree = toJson(component);
  });

  describe('Component Exists', () => {
    it('Should render the Items Component', () => {
      expect(component.length).toEqual(1);
    });
  });

  describe('updatePayload', () => {
    it('should update payload with expected', () => {
      const { data } = props;
      const expected = {
        username: data.username,
        password: data.password
      };

      const payload = component.instance().constructor.updatePayload(data);
      expect(payload).toEqual(expected);
    });
  });

  describe('validate', () => {
    it('should return true if values are missing', () => {
      const { data } = props;

      const valid = component.instance().constructor.validate(data);
      const invalid = component.instance().constructor.validate({
        ...data,
        username: ''
      });

      expect(valid).toBe(false);
      expect(invalid).toBe(true);
    });
  });

  describe('onUpdate', () => {
    it('should call props.update with ecpected', () => {
      const field = 'username';
      const value = 'test@test.com';
      const expected = { [field]: value };

      component.instance().onUpdate(field, value);
      expect(props.update).toHaveBeenCalledWith(expected);
    });
  });

  describe('submitLogin', () => {
    it('should call props.submit() with expected', () => {
      const expected = component.instance().payload;
      component.instance().submitLogin(new Event('click'));
      expect(props.submit).toHaveBeenCalledWith(expected);
    });
  });

  describe('Snapshot Match', () => {
    it('Should match previous snapshot', () => {
      expect(tree).toMatchSnapshot();
    });
  });
});
