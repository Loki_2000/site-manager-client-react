import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import Input from '../../../widgets/input';
import Select from '../../../widgets/select';
import Button from '../../../widgets/button';

class AddItem extends Component {
  static propTypes = {
    loading: PropTypes.bool.isRequired,
    material: PropTypes.string.isRequired,

    types: PropTypes.objectOf(PropTypes.objectOf(PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.bool,
      PropTypes.array
    ]))).isRequired,

    data: PropTypes.PropTypes.objectOf(PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ])).isRequired,

    updateFormData: PropTypes.func.isRequired,
    setDefaultData: PropTypes.func.isRequired,
    resetFormData: PropTypes.func.isRequired,
    addItem: PropTypes.func.isRequired
  }

  static updatePayload = data => Object.keys(data)
    .reduce((obj, key) => {
      if (data[key]) {
        return {
          ...obj,
          [key]: data[key]
        };
      }
      return obj;
    }, {});

  static validate = (data) => {
    if (!data.material) return true;

    const missingValues = !!Object.values(data)
      .filter(v => !v).length;

    return missingValues;
  }

  constructor(props) {
    super(props);
    this.initialiseData(this.props.material);
  }

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.material !== this.props.material) {
      this.initialiseData(nextProps.material);
    }
  }

  shouldComponentUpdate = nextProps => (
    nextProps.data !== this.props.data ||
    nextProps.loading !== this.props.loading
  );

  onUpdate = (field, value) => {
    const shouldBeInteger = field === 'length'
      || field === 'quantity';

    this.props.updateFormData({
      [field]: shouldBeInteger ? parseInt(value, 10) : value
    });
  }

  initialiseData = (material) => {
    this.props.resetFormData({});
    this.props.setDefaultData(material);
  }

  parseOptions = (field) => {
    const { types, data } = this.props;

    let options = [];

    switch (field) {
    case 'material':
      options = Object.keys(types)
        .map(id => types[id][field]);

      break;
    case 'types':
    case 'grades':
    case 'diameters':
    case 'densities':
    case 'finishes':
    case 'strengths':
    case 'blocksizes':
      options = Object.keys(types)
        .filter(key => types[key].material === data.material)
        .map(id => types[id][field]);

      break;
    case 'sections':
      if (data.material !== 'steel') return [];
      if (!data.type) return ['Please Choose Type'];

      options = Object.keys(types)
        .map(id => types[id])
        .find(obj => obj.type === data.type).sections;

      break;
    default:
    }
    // Ensure returned array is flattened with uniques values
    return [...new Set([].concat(...options))];
  }

  submitItem = () => {
    this.props.addItem(this.payload);
  }

  render = () => {
    const { data } = this.props;
    const { material } = data;
    const validated = this.constructor.validate(data);

    this.payload = this.constructor.updatePayload(data);

    return (
      <div>
        <Select
          selected={data.diameter || ''}
          display={material === 'rebar'}
          field="diameter"
          label="Diameter"
          options={this.parseOptions('diameters')}
          handleChange={this.onUpdate}
        />
        <Select
          selected={data.type || ''}
          display={['steel', 'brick', 'timber'].includes(material)}
          field="type"
          label="Type"
          options={this.parseOptions('types')}
          handleChange={this.onUpdate}
        />
        <Select
          selected={data.density || ''}
          display={material === 'block'}
          field="density"
          label="Density"
          options={this.parseOptions('densities')}
          handleChange={this.onUpdate}
        />
        <Select
          selected={data.strength || ''}
          display={['block', 'brick'].includes(material)}
          field="strength"
          label="Strength"
          options={this.parseOptions('strengths')}
          handleChange={this.onUpdate}
        />
        <Select
          selected={data.section || ''}
          display={material === 'steel'}
          field="section"
          label="Section"
          options={this.parseOptions('sections')}
          handleChange={this.onUpdate}
        />
        <Select
          selected={data.grade || ''}
          display={['steel', 'timber'].includes(material)}
          field="grade"
          label="Grade"
          options={this.parseOptions('grades')}
          handleChange={this.onUpdate}
        />
        <Select
          selected={data.blocksize || ''}
          display={material === 'block'}
          field="blocksize"
          label="Size"
          options={this.parseOptions('blocksizes')}
          handleChange={this.onUpdate}
        />
        <Select
          selected={data.finish || ''}
          display={material === 'block'}
          field="finish"
          label="Finish"
          options={this.parseOptions('finishes')}
          handleChange={this.onUpdate}
        />
        <Input
          display={['steel', 'timber', 'rebar'].includes(material)}
          field="length"
          id="length"
          value={data.length || ''}
          type="number"
          placeholder="Length"
          handleChange={this.onUpdate}
        />
        <Input
          display={material !== 'other'}
          field="quantity"
          id="quantity"
          value={data.quantity || ''}
          type="number"
          placeholder="Quantity"
          handleChange={this.onUpdate}
        />
        <Select
          selected={data.job_number || ''}
          field="job_number"
          id="job_number"
          label="Job No."
          options={['1900', '0076']}
          handleChange={this.onUpdate}
        />
        <Input
          field="project"
          id="project"
          value={data.project || ''}
          type="text"
          placeholder="Project Name"
          handleChange={this.onUpdate}
        />
        <Button
          loading={this.props.loading}
          dispatch={this.submitItem}
          label="Add Item"
          id="add-item"
          theme="light"
          disabled={validated}
        />
      </div>
    );
  }
}

export default AddItem;
