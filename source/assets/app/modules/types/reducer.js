import { LOAD_TYPES } from './actions';

export const typesReducer = (state = {}, { type, payload }) => {
  switch (type) {
  case LOAD_TYPES: {
    const { types } = payload;

    return {
      ...state,
      ...types
    };
  }
  default: {
    return state;
  }
  }
};

export const typeIDsReducer = (state = [], { type, payload }) => {
  switch (type) {
  case LOAD_TYPES: {
    const { typeIDs } = payload;

    return typeIDs;
  }
  default: {
    return state;
  }
  }
};
