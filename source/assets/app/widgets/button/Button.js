import React from 'react';
import PropTypes from 'prop-types';

import './Button.scss';

const Button = ({
  dispatch, label, id, icon, loading, theme, type, disabled
}) => {
  const buttonContent = () => {
    if (loading) {
      return (
        <img alt="loader" src="/assets/images/loading.svg" />
      );
    }
    if (icon) {
      return (
        <div>
          {label}
          <i id={id} className={`${icon} icon`} aria-hidden="true" />
        </div>
      );
    }
    return label;
  };

  return (
    <div theme={theme} className="input">
      <button
        disabled={disabled}
        type={type}
        id={id}
        onClick={((e) => {
          e.preventDefault();
          dispatch(id, e);
        })}
      >
        {buttonContent()}
      </button>
    </div>
  );
};

export default Button;

Button.propTypes = {
  dispatch: PropTypes.func.isRequired,
  label: PropTypes.string,
  id: PropTypes.string.isRequired,
  icon: PropTypes.string,
  loading: PropTypes.bool,
  theme: PropTypes.string,
  type: PropTypes.string,
  disabled: PropTypes.bool
};

Button.defaultProps = {
  label: undefined,
  icon: undefined,
  loading: false,
  theme: '',
  type: 'submit',
  disabled: false
};
