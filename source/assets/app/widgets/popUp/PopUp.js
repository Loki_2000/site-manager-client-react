import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import './PopUp.scss';

const PopUp = ({ options }) => (
  <div className="pop-up">
    <ul>
      {
        options.map(option => (
          <li
            key={option}
            id={option}
            className="item"
          >
            <Link
              to={{
                pathname: `/items/${option}/add`,
                state: { material: option }
              }}
            >
              {option}
            </Link>
          </li>))
      }
    </ul>
  </div>
);

export default PopUp;

PopUp.propTypes = {
  options: PropTypes.arrayOf(PropTypes.string).isRequired
};
