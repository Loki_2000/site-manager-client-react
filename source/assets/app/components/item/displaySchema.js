export const brick = {
  titles: ['type', 'strength'],
  details: ['quantity']
};

export const block = {
  titles: ['density', 'strength', 'finish'],
  details: ['blocksize', 'quantity']
};

export const rebar = {
  titles: ['material', 'diameter'],
  details: ['length', 'quantity']
};

export const steel = {
  titles: ['type', 'section', 'grade'],
  details: ['length', 'quantity']
};

export const timber = {
  titles: ['type', 'grade'],
  details: ['length', 'quantity']
};
