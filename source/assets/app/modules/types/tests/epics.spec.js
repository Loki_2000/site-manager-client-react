import fetchMock from 'fetch-mock';
import configureMockStore from 'redux-mock-store';
import { createEpicMiddleware } from 'redux-observable';
import LocalStorageMock from '../../../../tests';
import getTypesEpic from '../epics';
import { setTimeout } from 'timers';

const epicMiddleware = createEpicMiddleware(getTypesEpic);
const mockStore = configureMockStore([epicMiddleware]);


describe('Types Epic', () => {
  let store;
  global.localStorage = new LocalStorageMock();

  const typeID = '09TR';
  const res = [{
    _id: typeID,
    type: 'UC',
    grades: ['S275', 'S355'],
    sections: ['356x406x634'],
    types: ['UB']
  }];

  const normalized = {
    typeIDs: [typeID],
    types: {
      [typeID]: {
        _id: typeID,
        type: 'UC',
        grades: ['S275', 'S355'],
        sections: ['356x406x634'],
        types: ['UB']
      }
    }
  };

  beforeEach(() => {
    store = mockStore();
  });

  afterEach(() => {
    fetchMock.restore();
    epicMiddleware.replaceEpic(getTypesEpic);
  });

  describe('getTypesEpic', () => {
    it('should request types and return with the expected list of actions', (done) => {
      const expected = [{
        type: 'types/GET_TYPES'
      }, {
        type: 'types/LOAD_TYPES',
        payload: normalized
      }];

      fetchMock.get('/api/steel_types', { ...res });

      store.dispatch({
        type: 'types/GET_TYPES'
      });

      setTimeout(() => {
        expect(store.getActions()).toEqual(expected);
        done();
      });
    });
  });
});
