import 'rxjs';
import { Observable } from 'rxjs/Observable';

import {
  REQUEST_LOGIN,
  CHECK_LOGIN,
  userLoggedIn
} from './actions';

import { updateLayout } from '../layout/actions';
import { resetFormData } from '../formData/actions';

import * as api from './api';

export const sendLoginRequest = action$ =>
  action$.ofType(REQUEST_LOGIN)
    .switchMap((action) => {
      const apiRequest$ = Observable.fromPromise(api.requestLogin(action.payload))
        .map(user => userLoggedIn(user))
        .catch(error => Observable.of({
          type: 'AJAX_ERROR',
          payload: error,
          error: true
        }));

      return Observable.concat(
        Observable.of(updateLayout({ loading: true })),
        apiRequest$,
        Observable.of(updateLayout({ loading: false })),
        Observable.of(resetFormData({}))
      );
    });


export const checkUserLogin = action$ =>
  action$.ofType(CHECK_LOGIN)
    .switchMap(action => api.checkLogin(action.id))
    .map(user => userLoggedIn(user))
    .catch(error => Observable.of({
      type: 'AJAX_ERROR',
      payload: error,
      error: true
    }));
