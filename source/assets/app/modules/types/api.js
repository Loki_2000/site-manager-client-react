import { normalize } from 'normalizr';

import { typeSchema } from '../../schemas';

const getTypes = () => {
  const url = '/api/steel_types';
  const OPTIONS = {
    credentials: 'same-origin',
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'x-access-token': localStorage.getItem('token')
    }
  };

  return fetch(url, OPTIONS)
    .then((response) => {
      if (!response.ok) {
        return Promise.reject(response);
      }
      return Promise.resolve(response.json());
    })
    .then((json) => {
      const data = { types: json };
      const { entities, result } = normalize(data, typeSchema);

      return {
        types: entities.types,
        typeIDs: result.types
      };
    });
};

export default getTypes;

