import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';

import Layout from '../../components/layout';
import { updateLayout, resetLayout } from '../../modules/layout/actions';
import { getTypes } from '../../modules/types/actions';

const mapDispatchToProps = dispatch => (bindActionCreators({
  updateLayout,
  resetLayout,
  getTypes
}, dispatch));

const mapStateToProps = state => ({
  layout: state.layout
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Layout));
