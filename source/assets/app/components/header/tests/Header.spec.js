import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Header from '../';

describe('Header Component', () => {
  let component;
  let tree;

  beforeEach(() => {
    component = shallow(<Header title="title" />);
    tree = toJson(component);
  });

  describe('Component Exists', () => {
    it('Should render the Header Component', () => {
      expect(component.length).toEqual(1);
    });
  });

  describe('Snapshot Match', () => {
    it('Should match previous snapshot', () => {
      expect(tree).toMatchSnapshot();
    });
  });
});
