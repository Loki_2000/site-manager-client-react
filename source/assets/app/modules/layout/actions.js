export const UPDATE_LAYOUT = 'layout/UPDATE_LAYOUT';
export const RESET_LAYOUT = 'layout/RESET_LAYOUT';

export const updateLayout = layout => ({
  type: UPDATE_LAYOUT,
  payload: { layout }
});

export const resetLayout = () => ({
  type: RESET_LAYOUT
});
