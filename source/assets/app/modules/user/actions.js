export const REQUEST_LOGIN = 'user/REQUEST_LOGIN';
export const USER_LOGGED_IN = 'user/USER_LOGGED_IN';
export const CHECK_LOGIN = 'user/CHECK_LOGIN';

export const requestLogin = details => ({
  type: REQUEST_LOGIN,
  payload: details
});

export const userLoggedIn = user => ({
  type: USER_LOGGED_IN,
  payload: {
    user
  }
});

export const checkLogin = id => ({
  type: CHECK_LOGIN,
  id
});
