export const brick = {
  material: 'brick',
  type: '',
  strength: '',
  quantity: null,
  job_number: '',
  project: ''
};

export const block = {
  material: 'block',
  density: '',
  strength: '',
  blocksize: '',
  finish: '',
  quantity: null,
  job_number: '',
  project: ''
};

export const rebar = {
  material: 'rebar',
  diameter: '',
  length: null,
  quantity: null,
  job_number: '',
  project: ''
};

export const steel = {
  material: 'steel',
  type: '',
  section: '',
  grade: '',
  length: null,
  quantity: null,
  job_number: '',
  project: ''
};

export const timber = {
  material: 'timber',
  type: '',
  grade: '',
  length: null,
  quantity: null,
  job_number: '',
  project: ''
};

export const login = {
  username: '',
  password: ''
};

export const order = {
  quantity: null,
  job_number: '',
  project: ''
};
