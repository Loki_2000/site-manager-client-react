import { typesReducer, typeIDsReducer } from '../reducer';

describe('Types Reducers', () => {
  const typeID = '09TR';
  const payload = {
    typeIDs: [typeID],
    types: {
      [typeID]: {
        type: 'UC',
        grades: ['S275', 'S355'],
        sections: ['356x406x634'],
        types: ['UB']
      }
    }
  };

  describe('LOAD_TYPES', () => {
    const initialState = Object.freeze({});
    it('should refresh state with expected types', () => {
      const state = typesReducer(initialState, {
        type: 'types/LOAD_TYPES',
        payload
      });

      expect(state).toEqual({ ...initialState, ...payload.types });
    });
  });

  describe('LOAD_TYPES: typeIDs', () => {
    const initialState = Object.freeze(['0365']);

    it('should refresh state with expected typesIDs', () => {
      const state = typeIDsReducer(initialState, {
        type: 'types/LOAD_TYPES',
        payload
      });

      expect(state).toEqual(payload.typeIDs);
    });
  });
});
