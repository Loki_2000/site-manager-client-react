import formDataReducer from '../reducer';

describe('FormData Reducer', () => {
  const payload = {
    username: 'Terence',
    password: 'test'
  };

  describe('formData/UPDATE_FORM_DATA', () => {
    const initialState = Object.freeze({});

    it('should update state with expected payload', () => {
      const state = formDataReducer(initialState, {
        type: 'formData/UPDATE_FORM_DATA',
        payload
      });

      expect(state).toEqual({ ...initialState, ...payload.items });
    });
  });

  describe('formData/RESET_FORM_DATA', () => {
    const initialState = Object.freeze({});

    it('should update state with expected payload', () => {
      const state = formDataReducer(initialState, {
        type: 'formData/RESET_FORM_DATA'
      });

      expect(state).toEqual({ ...initialState });
    });
  });
});
