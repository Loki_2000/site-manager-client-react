module.exports = {
  "parser": "babel-eslint",
  "rules": {
    "strict": 0
  },
  "env": {
    "browser": true,
    "es6": true,
    "jquery": true
  },
  "extends": "airbnb",
  "parserOptions": {
    "sourceType": "module",
    "ecmaFeatures": {
      "jsx": true
    }
  },
  "plugins": [
    "react"
  ],
  "globals": {
    "describe": true,
    "jest": true,
    "expect": true,
    "it": true,
    "beforeEach": true,
    "spyOn": true,
    "afterEach": true
  },
  "rules": {
    "comma-dangle": [
      "warn",
      "never"
    ],
    "import/no-extraneous-dependencies": [
      "error",
      { "devDependencies": true }
    ],
    "no-underscore-dangle": [
      "error", { "allow": ["_id"] }
    ],
    "react/jsx-filename-extension": [
      1,
      { "extensions": [".js", ".jsx"] }
    ],
    "jsx-a11y/anchor-is-valid": ["error", {
      "specialLink": ["Link"]
    }],
    "indent": [
      "warn",
      2
    ],
    "linebreak-style": [
      "warn",
      "unix"
    ],
    "quotes": [
      "error",
      "single"
    ],
    "semi": [
      "error",
      "always"
    ],
    /* Advanced Rules*/
    "no-unused-expressions": "warn",
    "no-useless-concat": "warn",
    "block-scoped-var": "error",
    "consistent-return": "error"
  }
};