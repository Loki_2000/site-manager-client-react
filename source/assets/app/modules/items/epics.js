import 'rxjs';
import { Observable } from 'rxjs/Observable';
import { updateLayout } from '../layout/actions';
import { resetFormData } from '../formData/actions';
import { GET_ITEMS, ADD_ITEM, ORDER_ITEM, loadItems, loadItem, actionTest } from './actions';

import * as api from './api';

export const getItemsEpic = action$ =>
  action$.ofType(GET_ITEMS)
    .switchMap(() => api.getItems())
    .map(items => loadItems(items))
    .catch(error => Observable.of({
      type: 'AJAX_ERROR',
      payload: error,
      error: true
    }));

export const addItemsEpic = action$ =>
  action$.ofType(ADD_ITEM)
    .switchMap((action) => {
      const apiRequest$ = Observable.fromPromise(api.addItem(action.payload))
        .map(item => loadItem(item))
        .catch(error => Observable.of({
          type: 'AJAX_ERROR',
          payload: error,
          error: true
        }));

      return Observable.concat(
        Observable.of(updateLayout({ loading: true })),
        apiRequest$,
        Observable.of(updateLayout({ loading: false })),
        Observable.of(resetFormData({}))
      );
    });

export const orderItemEpic = action$ =>
  action$.ofType(ORDER_ITEM)
    .switchMap((action) => {
      const { order, item } = action.payload;

      const apiRequest$ = Observable.fromPromise(api.createOrder(order))
        .map(() => actionTest())
        .catch(error => Observable.of({
          type: 'AJAX_ERROR',
          payload: error,
          error: true
        }));

      const updateItem$ = Observable.fromPromise(api.updateItem(item))
        .map(() => actionTest())
        .catch(error => Observable.of({
          type: 'AJAX_ERROR',
          payload: error,
          error: true
        }));

      return Observable.concat(
        Observable.of(updateLayout({ loading: true })),
        apiRequest$,
        updateItem$,
        Observable.of(updateLayout({ loading: false })),
        Observable.of(resetFormData({}))
      );
    });
