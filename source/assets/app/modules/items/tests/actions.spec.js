import {
  getItems,
  getItem,
  loadItems,
  loadItem,
  addItem,
  selectItem,
  orderItem
} from '../actions';

describe('Items Actions', () => {
  const selectedItemID = '09TR';
  const items = {
    [selectedItemID]: {
      _id: '098765dfgh',
      type: 'Equal Angles'
    }
  };

  describe('getItems()', () => {
    it('should return an obj with type: "items/GET_ITEMS"', () => {
      const expected = 'items/GET_ITEMS';
      const action = getItems();
      expect(action.type).toEqual(expected);
    });
  });

  describe('getItem()', () => {
    it('should return an obj with type: "items/GET_ITEM", id: 123', () => {
      const id = '123';
      const expected = {
        type: 'items/GET_ITEM',
        id
      };

      const action = getItem(id);
      expect(action).toEqual(expected);
    });
  });

  describe('loadItems()', () => {
    it('should return an obj {type: "items/LOAD_ITEMS", payload: { ... }', () => {
      const payload = { ...items };
      const expected = {
        type: 'items/LOAD_ITEMS',
        payload
      };

      const action = loadItems(payload);
      expect(action).toEqual(expected);
    });
  });

  describe('loadItem()', () => {
    it('should return an obj {type: "items/LOAD_ITEM", payload: { ... }', () => {
      const payload = items[selectedItemID];
      const expected = {
        type: 'items/LOAD_ITEM',
        payload
      };

      const action = loadItem(payload);
      expect(action).toEqual(expected);
    });
  });

  describe('addItem()', () => {
    it('should return an obj {type: "items/ADD_ITEM", payload: { ... }', () => {
      const payload = items[selectedItemID];
      const expected = {
        type: 'items/ADD_ITEM',
        payload
      };

      const action = addItem(payload);
      expect(action).toEqual(expected);
    });
  });

  describe('selectItem()', () => {
    it('should return an obj {type: "items/SELECT_ITEM", payload: { ... }', () => {
      const id = selectedItemID;
      const expected = {
        type: 'items/SELECT_ITEM',
        id
      };

      const action = selectItem(id);
      expect(action).toEqual(expected);
    });
  });

  describe('orderItem()', () => {
    it('should return an obj {type: "items/ORDER_ITEM", payload: { ... }', () => {
      const payload = items[selectedItemID];
      const expected = {
        type: 'items/ORDER_ITEM',
        payload
      };

      const action = orderItem(payload);
      expect(action).toEqual(expected);
    });
  });
});
