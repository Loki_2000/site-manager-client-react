import 'rxjs';
import { Observable } from 'rxjs/Observable';

import { GET_TYPES, loadTypes } from './actions';

import getTypes from './api';

const getTypesEpic = action$ =>
  action$.ofType(GET_TYPES)
    .switchMap(() => getTypes())
    .map(types => loadTypes(types))
    .catch(error => Observable.of({
      type: 'AJAX_ERROR',
      payload: error,
      error: true
    }));

export default getTypesEpic;
