import React, { Component } from 'react';
import { PropTypes } from 'prop-types';

import { login as defaultLogin } from '../formInterfaces';
import Input from '../../../widgets/input';
import Button from '../../../widgets/button';

class Login extends Component {
  static propTypes = {
    data: PropTypes.objectOf(PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ])).isRequired,
    loading: PropTypes.bool.isRequired,
    update: PropTypes.func.isRequired,
    submit: PropTypes.func.isRequired
  }

  static updatePayload = data => Object.keys(data)
    .reduce((obj, key) => {
      if (data[key]) {
        return {
          ...obj,
          [key]: data[key]
        };
      }
      return obj;
    }, {});

  static validate = (data) => {
    const obj = {
      ...defaultLogin,
      ...data
    };

    const missingValues = !!Object.values(obj)
      .filter(v => !v).length;

    return missingValues;
  }

  onUpdate = (field, value) => {
    this.props.update({
      [field]: value
    });
  }

  submitLogin = () => {
    this.props.submit(this.payload);
  }

  render = () => {
    const { data } = this.props;
    const validated = this.constructor.validate(data);
    this.payload = this.constructor.updatePayload(data);

    return (
      <div>
        <Input
          id="username"
          value={data.username || ''}
          type="text"
          placeholder="Username"
          handleChange={this.onUpdate}
          theme="light"
        />
        <Input
          id="password"
          value={data.password || ''}
          type="password"
          placeholder="Password"
          handleChange={this.onUpdate}
          theme="light"
        />
        <Button
          loading={this.props.loading}
          dispatch={this.submitLogin}
          label="Log In"
          id="login"
          theme="dark"
          disabled={validated}
        />
      </div>
    );
  }
}

export default Login;
