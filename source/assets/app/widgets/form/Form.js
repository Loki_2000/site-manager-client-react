import React, { Component } from 'react';
import { PropTypes } from 'prop-types';

import AddItem from '../../containers/forms/addItem';
import OrderItem from '../../containers/forms/orderItem';
import Login from '../../containers/forms/login';
import FilterItems from '../../containers/forms/filterItems';


import './Form.scss';

class Form extends Component {
  switchForm() {
    switch (this.props.type) {
    case 'add-item-form':
      return (
        <AddItem material={this.props.material} />
      );
    case 'filter-items-form':
      return (
        <FilterItems />
      );

    case 'login-form':
      return (
        <Login />
      );

    case 'order-item-form':
      return (
        <OrderItem />
      );

    default:
      return ('');
    }
  }

  render() {
    return (
      <form className={this.props.type}>
        { this.switchForm() }
      </form>
    );
  }
}

export default Form;

Form.propTypes = {
  type: PropTypes.string.isRequired
};
