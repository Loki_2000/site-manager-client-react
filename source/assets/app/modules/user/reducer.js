import _ from 'lodash';

import { USER_LOGGED_IN } from './actions';

const User = {
  id: null,
  username: '',
  firstname: '',
  lastname: '',
  phone: '',
  isAdmin: false,
  isLoggedIn: false
};

const keys = Object.keys(User);

export default (state = User, { type, payload }) => {
  switch (type) {
  case USER_LOGGED_IN: {
    const user = _.pick(payload.user, keys);

    return { ...user, isLoggedIn: true };
  }
  default: {
    return state;
  }
  }
};
