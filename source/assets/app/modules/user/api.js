export const requestLogin = (payload) => {
  const url = '/api/security';

  const OPTIONS = {
    credentials: 'same-origin',
    method: 'POST',
    body: JSON.stringify(payload),
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
  };

  return fetch(url, OPTIONS)
    .then((response) => {
      if (!response.ok) {
        return Promise.reject(response);
      }
      return Promise.resolve(response.json());
    })
    .then((json) => {
      const { user } = json;
      const id = user._id;

      // Using this to persist the session on refresh
      localStorage.setItem('token', json.token);
      localStorage.setItem('userId', id);

      return {
        ...user,
        id
      };
    });
};

export const checkLogin = (id) => {
  const url = `/api/security/${id}`;

  const OPTIONS = {
    credentials: 'same-origin',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'x-access-token': localStorage.getItem('token')
    }
  };

  return fetch(url, OPTIONS)
    .then((response) => {
      if (!response.ok) {
        return Promise.reject(response);
      }
      return Promise.resolve(response.json());
    })
    .then((json) => {
      const { user } = json;
      return {
        ...user,
        id: user._id
      };
    });
};
