import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Account from '../';

describe('Items Component', () => {
  let component;
  let tree;

  beforeEach(() => {
    component = shallow(<Account />);
    tree = toJson(component);
  });

  describe('Component Exists', () => {
    it('Should render the Items Component', () => {
      expect(component.length).toEqual(1);
    });
  });

  describe('Snapshot Match', () => {
    it('Should match previous snapshot', () => {
      expect(tree).toMatchSnapshot();
    });
  });
});

