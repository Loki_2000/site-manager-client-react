import { getTypes, loadTypes } from '../actions';

describe('Layout Actions', () => {
  const typeID = '09TR';
  const types = {
    typeIds: [typeID],
    types: {
      [typeID]: {
        type: 'UC',
        grades: ['S275', 'S355'],
        sections: ['356x406x634'],
        types: ['UB']
      }
    }
  };

  describe('getTypes', () => {
    it('should call getTypes with expected action-type', () => {
      const expected = 'types/GET_TYPES';

      const action = getTypes(types);
      expect(action.type).toEqual(expected);
    });
  });

  describe('loadTypes', () => {
    it('should call loadTypes with expected action', () => {
      const expected = {
        type: 'types/LOAD_TYPES',
        payload: { ...types }
      };

      const action = loadTypes(types);
      expect(action).toEqual(expected);
    });
  });
});
