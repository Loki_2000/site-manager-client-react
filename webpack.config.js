const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');

module.exports = {
  entry: {
    app: [
      'babel-polyfill',
      'whatwg-fetch',
      './source/assets/app'
    ],
    tests: [
      'babel-polyfill',
      'whatwg-fetch',
      './source/assets/tests'
    ]
  },
  output: {
    filename: '[name].js',
    path: `${__dirname}/build/assets`,
    publicPath: '/assets/'
  },
  devtool: 'eval',
  module: {
    loaders: [
      {
        test: /\.svg$/,
        loader: 'babel-loader?presets[]=es2017,presets[]=react!svg-react-loader'
      },
      {
        test: /\.json$/,
        loader: 'json-loader'
      },
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader'
      },
      { test: /\.css$/, loader: 'style-loader!css-loader' },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader'
            },
            {
              loader: 'sass-loader'
            }
          ]
        }),
      },
      { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'url-loader' },
      { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'url-loader' }
    ],
  },
  plugins: [
    new ExtractTextPlugin('stylesheets/app.css'),
    new webpack.LoaderOptionsPlugin({
      options: {
        postcss: () => [autoprefixer({
          browsers: [
            'last 2 Chrome versions',
            'last 2 Firefox versions',
            'last 2 edge versions',
            'ie 11',
            'last 3 Safari versions',
            'Safari >= 8'
          ]
        })],
        progress: true
      }
    })
  ],
  resolve: {
    extensions: ['.webpack.js', '.web.js', '.js', '.es6.js', '.json', '.jsx', '.js.erb', '.css']
  }
};
