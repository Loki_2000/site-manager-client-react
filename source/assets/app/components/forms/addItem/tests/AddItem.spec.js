import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import AddItem from '../';

describe('Items Component', () => {
  let component;
  let tree;

  const typeIDs = ['steel', 'timber', 'brick', 'block', 'rebar'];
  const props = {
    types: {
      [typeIDs[0]]: {
        material: 'steel',
        type: 'UC',
        grades: ['S275', 'S355'],
        sections: ['356x406x634'],
        types: ['UB']
      },
      [typeIDs[1]]: {
        material: 'timber',
        grades: ['C16', 'C22'],
        types: ['5mm PLY', '18mm PLY']
      },
      [typeIDs[2]]: {
        material: 'brick',
        strengths: ['3.5N', '5N'],
        types: ['Flettons', 'Glazed']
      },
      [typeIDs[3]]: {
        material: 'block',
        strengths: ['3.5N', '5N'],
        finishes: ['Coarse', 'Paint'],
        blocksizes: ['200x3', '400x2'],
        densities: ['Heavy', 'Semi']
      },
      [typeIDs[4]]: {
        material: 'rebar',
        diameters: ['H8', 'H12']
      }
    },
    data: {
      type: '',
      section: '',
      grade: '',
      length: 3,
      quantity: 0,
      job_number: 1234,
      project: '',
      diameter: '',
      strength: '',
      blocksize: '',
      density: '',
      material: '',
      finish: ''
    },
    updateFormData: jest.fn(),
    resetFormData: jest.fn(),
    setDefaultData: jest.fn(),
    addItem: jest.fn()
  };

  beforeEach(() => {
    component = shallow(<AddItem {...props} />);
    tree = toJson(component);
  });

  describe('Component Exists', () => {
    it('Should render the Items Component', () => {
      expect(component.length).toEqual(1);
    });
  });

  describe('updatePayload', () => {
    it('should update payload with expected', () => {
      const { data } = props;
      const expected = {
        length: data.length,
        job_number: data.job_number
      };

      const payload = component.instance().constructor.updatePayload(data);
      expect(payload).toEqual(expected);
    });
  });

  describe('validate', () => {
    it('should return true if values are missing', () => {
      const data = {
        material: 'steel',
        type: 'Equal Angles',
        section: '',
        grade: 'C255',
        length: 3,
        quantity: 2,
        job_number: 1234,
        project: 'test'
      };

      const invalid = component.instance().constructor.validate(data);
      const valid = component.instance().constructor.validate({
        ...data,
        section: '200'
      });

      expect(valid).toBe(false);
      expect(invalid).toBe(true);
    });
  });

  describe('initialiseData', () => {
    it('should call expected functions', () => {
      component.instance().initialiseData('steel');
      expect(props.resetFormData).toHaveBeenCalledWith({});
      expect(props.setDefaultData).toHaveBeenCalledWith('steel');
    });
  });

  describe('onUpdate', () => {
    it('should call props.update with ecpected', () => {
      let field = 'type';
      let value = 'UB';
      let expected = { [field]: value };

      component.instance().onUpdate(field, value);
      expect(props.updateFormData).toHaveBeenCalledWith(expected);

      field = 'length';
      value = '2';
      expected = { [field]: 2 };

      component.instance().onUpdate(field, value);
      expect(props.updateFormData).toHaveBeenCalledWith(expected);
    });
  });

  describe('parseOptions', () => {
    it('should loop through material types and return expected array of options', (done) => {
      let expected;
      let parseOptions;

      typeIDs.forEach((material) => {
        props.data.material = material;
        props.data.type = 'UC';

        Object.keys(props.types[material])
          .filter(key => Array.isArray(props.types[material][key]))
          .forEach((key) => {
            expected = props.types[material][key];
            parseOptions = component.instance().parseOptions(key);

            expect(parseOptions).toEqual(expected);
          });
      });
      // Reseting changed props
      props.data.material = '';
      props.data.type = '';
      done();
    });
  });

  describe('submitItem', () => {
    it('should call props.submit() with expected', () => {
      const expected = component.instance().payload;
      component.instance().submitItem(new Event('click'));
      expect(props.addItem).toHaveBeenCalledWith(expected);
    });
  });

  describe('Snapshot Match', () => {
    it('Should match previous snapshot', () => {
      expect(tree).toMatchSnapshot();
    });
  });
});
