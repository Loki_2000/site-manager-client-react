import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { FilterItems } from '../../../components/forms';
import { setDefaultData, updateFormData } from '../../../modules/formData/actions';
import { updateLayout } from '../../../modules/layout/actions';

const mapDispatchToProps = dispatch => (bindActionCreators({
  setDefaultData,
  updateFormData,
  updateLayout
}, dispatch));

const mapStateToProps = state => ({
  types: state.types,
  filters: state.layout.filters
});

export default connect(mapStateToProps, mapDispatchToProps)(FilterItems);
