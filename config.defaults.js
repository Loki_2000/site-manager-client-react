module.exports = {
  port: 8001,
  apiHost: 'http://localhost:8080',
  apiPath: '/api',
  livereload: true
};
