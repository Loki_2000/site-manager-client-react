import fetchMock from 'fetch-mock';
import configureMockStore from 'redux-mock-store';
import { createEpicMiddleware, combineEpics } from 'redux-observable';
import LocalStorageMock from '../../../../tests';
import { getItemsEpic, addItemsEpic, orderItemEpic } from '../epics';
import { setTimeout } from 'timers';

const epics = combineEpics(getItemsEpic, addItemsEpic, orderItemEpic);
const epicMiddleware = createEpicMiddleware(epics);
const mockStore = configureMockStore([epicMiddleware]);


describe('Items Epic', () => {
  let store;
  global.localStorage = new LocalStorageMock();

  const itemID = '09TR';
  const res = [{
    _id: itemID,
    type: 'Equal Angles',
    section: '200x200',
    grade: 'S265',
    length: 7,
    quantity: 20
  }];

  const normalised = {
    items: {
      [itemID]: {
        _id: itemID,
        type: 'Equal Angles',
        section: '200x200',
        grade: 'S265',
        length: 7,
        quantity: 20
      }
    },
    itemIDs: [itemID]
  };

  beforeEach(() => {
    store = mockStore();
  });

  afterEach(() => {
    fetchMock.restore('*');
    epicMiddleware.replaceEpic(epics);
  });

  describe('getItemsEpic', () => {
    it('should request items and return with the expected list of actions', (done) => {
      const expected = [{
        type: 'items/GET_ITEMS'
      }, {
        type: 'items/LOAD_ITEMS',
        payload: normalised
      }];

      fetchMock.get('/api/items', { ...res });

      store.dispatch({
        type: 'items/GET_ITEMS'
      });

      setTimeout(() => {
        expect(store.getActions()).toEqual(expected);
        done();
      });
    });
  });

  describe('addItemsEpic', () => {
    it('should add an item and return with the expected action', (done) => {
      const expected = [{
        type: 'items/ADD_ITEM',
        payload: res[0]
      }, {
        type: 'layout/UPDATE_LAYOUT',
        payload: {
          layout: {
            loading: true
          }
        }
      }, {
        type: 'items/LOAD_ITEM',
        payload: res[0]
      }, {
        type: 'layout/UPDATE_LAYOUT',
        payload: {
          layout: {
            loading: false
          }
        }
      }, {
        type: 'formData/RESET_FORM_DATA',
        payload: {}
      }];

      fetchMock.post('/api/items/add', { ...res[0] });

      store.dispatch({
        type: 'items/ADD_ITEM',
        payload: res[0]
      });

      setTimeout(() => {
        expect(store.getActions()).toEqual(expected);
        done();
      });
    });
  });

  describe('orderItemEpic', () => {
    it('should request an order and return a list of expected actions', (done) => {
      const payload = {
        item: res[0],
        order: {
          type: 'Equal Angles',
          section: '200x200',
          grade: 'S265',
          length: 7,
          quantity: 20
        }
      };

      const expected = [{
        type: 'items/ORDER_ITEM',
        payload
      }, {
        type: 'layout/UPDATE_LAYOUT',
        payload: {
          layout: {
            loading: true
          }
        }
      }, {
        type: 'TEST_TEST'
      }, {
        type: 'TEST_TEST'
      }, {
        type: 'layout/UPDATE_LAYOUT',
        payload: {
          layout: {
            loading: false
          }
        }
      }, {
        type: 'formData/RESET_FORM_DATA',
        payload: {}
      }];

      fetchMock.post('/api/orders/add', payload.order);
      fetchMock.put(`/api/items/update/${itemID}`, { ...res[0] });

      store.dispatch({
        type: 'items/ORDER_ITEM',
        payload
      });

      setTimeout(() => {
        expect(store.getActions()).toEqual(expected);
        done();
      });
    });
  });
});
