export const GET_TYPES = 'types/GET_TYPES';
export const LOAD_TYPES = 'types/LOAD_TYPES';

export const getTypes = () => ({
  type: GET_TYPES
});

export const loadTypes = types => ({
  type: LOAD_TYPES,
  payload: {
    ...types
  }
});
