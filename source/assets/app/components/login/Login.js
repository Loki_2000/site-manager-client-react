import React from 'react';

import Form from '../../widgets/form';
import './Login.scss';

const Login = () => (
  <div className="app-login">
    <Form action="Login" type="login-form" />
  </div>
);

export default Login;
