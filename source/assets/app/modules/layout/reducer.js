import { UPDATE_LAYOUT } from './actions';
import { RESET_LAYOUT } from './actions';

const Layout = {
  modalOpen: false,
  theme: 'brand',
  section: '',
  loading: false,
  popUpOpen: false,
  popUp: '',
  filters: {}
};

export default (state = Layout, { type, payload }) => {
  switch (type) {
  case UPDATE_LAYOUT: {
    const { layout } = payload;

    return {
      ...state,
      ...layout
    };
  }
  case RESET_LAYOUT: {

    return Layout;
  }
  default: {
    return state;
  }
  }
};
