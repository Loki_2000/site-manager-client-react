import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import OrderItem from '../';

describe('Items Component', () => {
  let component;
  let tree;

  const props = {
    data: {
      grade: '',
      length: null,
      quantity: 3,
      job_number: 9876,
      project: 'test'
    },
    item: {
      _id: '5a0d7369fa457a1d6e762558',
      type: 'Equal Angles',
      section: '356x46x634',
      grade: 'S275',
      length: 12,
      quantity: 9
    },
    updateFormData: jest.fn(),
    resetFormData: jest.fn(),
    orderItem: jest.fn()
  };

  beforeEach(() => {
    component = shallow(<OrderItem {...props} />);
    tree = toJson(component);
  });

  describe('Component Exists', () => {
    it('Should render the Items Component', () => {
      expect(component.length).toEqual(1);
    });
  });

  describe('getStaticProperties', () => {
    it('should return the expected static properties of an order', () => {
      const expected = (({
        type, section, grade, length
      } = props.item) =>
        ({
          type, section, grade, length
        }))();

      expect(component.instance().constructor
        .getStaticProperties(props.item)).toEqual(expected);
    });
  });

  describe('getOrderDetails', () => {
    it('should return the input details of an order', () => {
      const expected = (({
        quantity, job_number, project
      } = props.data) =>
        ({
          quantity, job_number, project
        }))();

      expect(component.instance().constructor
        .getOrderDetails(props.data)).toEqual(expected);
    });
  });

  describe('validate', () => {
    it('should return false if no values are missing', () => {
      const valid = component.instance().constructor.validate({
        material: 'steel',
        type: 'Equal Angles',
        section: '356x46x634',
        grade: 'S275',
        length: 12,
        quantity: 9
      });

      expect(valid).toBe(false);
    });

    it('should return true if values are missing', () => {
      const invalid = component.instance().constructor.validate({
        material: 'steel',
        type: 'Equal Angles',
        section: '356x46x634',
        grade: 'S275',
        length: 12,
        quantity: 0
      });

      expect(invalid).toBe(true);
    });
  });

  describe('onUpdate', () => {
    it('should call updateFormData with expected values', () => {
      const field = 'quantity';
      const value = '7';
      const expected = { [field]: 7 };
      component.instance().onUpdate(field, value);

      expect(props.updateFormData).toHaveBeenCalledWith(expected);
    });
  });

  describe('initialiseData', () => {
    it('should call te expected functions to set correct data', () => {
      const expected = {
        type: 'Equal Angles',
        section: '356x46x634',
        grade: 'S275',
        length: 12,
        quantity: 9
      };

      component.instance().initialiseData();
      expect(props.resetFormData).toHaveBeenCalledWith({});
      expect(props.updateFormData).toHaveBeenCalledWith(expected);
    });
  });

  describe('submitOrder', () => {
    it('should call props.submit() with expected', () => {
      const expected = component.instance().payload;
      component.instance().submitOrder(new Event('click'));
      expect(props.orderItem).toHaveBeenCalledWith(expected);
    });
  });

  describe('Snapshot Match', () => {
    it('Should match previous snapshot', () => {
      expect(tree).toMatchSnapshot();
    });
  });
});
