export const GET_ITEMS = 'items/GET_ITEMS';
export const GET_ITEM = 'items/GET_ITEM';
export const LOAD_ITEMS = 'items/LOAD_ITEMS';
export const LOAD_ITEM = 'items/LOAD_ITEM';
export const ADD_ITEM = 'items/ADD_ITEM';
export const ORDER_ITEM = 'items/ORDER_ITEM';
export const SELECT_ITEM = 'items/SELECT_ITEM';

export const getItems = () => ({
  type: GET_ITEMS
});

export const getItem = id => ({
  type: GET_ITEM,
  id
});

export const loadItems = items => ({
  type: LOAD_ITEMS,
  payload: {
    ...items
  }
});

export const loadItem = item => ({
  type: LOAD_ITEM,
  payload: item
});

export const addItem = item => ({
  type: ADD_ITEM,
  payload: item
});

export const selectItem = id => ({
  type: SELECT_ITEM,
  id
});

export const orderItem = payload => ({
  type: ORDER_ITEM,
  payload
});

export const actionTest = () => ({
  type: 'TEST_TEST'
});
