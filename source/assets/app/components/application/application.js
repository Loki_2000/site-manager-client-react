import React, { Component } from 'react';
import { PropTypes } from 'prop-types';

import Login from '../login';
import Layout from '../../containers/layout';
import './Application.scss';

class Application extends Component {
  static propTypes = {
    checkLogin: PropTypes.func.isRequired,
    history: PropTypes.objectOf(PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.object,
      PropTypes.func
    ])).isRequired,
    user: PropTypes.objectOf(PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.bool
    ])).isRequired
  };

  isLoggedIn = () => {
    const { user, history } = this.props;

    if (!user.isLoggedIn && localStorage.getItem('token')
      && localStorage.getItem('userId')) {
      this.props.checkLogin(localStorage.getItem('userId'));
    }

    if (user.isLoggedIn) {
      return (
        <div>
          <Layout history={history} />
        </div>
      );
    }
    return (<Login />);
  }

  render = () => (
    <div className="app">
      {this.isLoggedIn()}
    </div>
  );
}

export default Application;
