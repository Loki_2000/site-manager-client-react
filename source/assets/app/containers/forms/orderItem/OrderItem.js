import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { setDefaultData, updateFormData, resetFormData } from '../../../modules/formData/actions';
import { orderItem } from '../../../modules/items/actions';
import { OrderItem } from '../../../components/forms';

const mapDispatchToProps = dispatch => (bindActionCreators({
  resetFormData,
  setDefaultData,
  updateFormData,
  orderItem
}, dispatch));

const mapStateToProps = state => ({
  loading: state.layout.loading,
  item: state.items[state.selectedItemID],
  data: state.formData
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderItem);
