import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import DatePicker from 'react-datepicker';
// import moment from 'moment';

import Input from '../../../widgets/input';
import Button from '../../../widgets/button';
import Select from '../../../widgets/select';

class OrderItem extends Component {
  static propTypes = {
    data: PropTypes.objectOf(PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.bool
    ])).isRequired,

    item: PropTypes.objectOf(PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.bool
    ])).isRequired,

    loading: PropTypes.bool.isRequired,
    updateFormData: PropTypes.func.isRequired,
    resetFormData: PropTypes.func.isRequired,
    orderItem: PropTypes.func.isRequired
  }

  static getStaticProperties = (item) => {
    const validProperties = [
      'type',
      'section',
      'grade',
      'length',
      'diameter',
      'material',
      'density',
      'strength',
      'blocksize',
      'finish'
    ];

    return validProperties.reduce((obj, key) => {
      if (item[key]) {
        return {
          ...obj,
          [key]: item[key]
        };
      }
      return obj;
    }, {});
  }

  static getOrderDetails = data => Object.keys(data)
    .reduce((obj, key) => {
      if (data[key]) {
        return {
          ...obj,
          [key]: data[key]
        };
      }
      return obj;
    }, {});

  static validate = (data) => {
    const missingValues = !!Object.values(data)
      .filter(v => !v).length;

    return missingValues;
  }

  constructor(props) {
    super(props);
    this.initialiseData();
  }

  onUpdate = (field, value) => {
    this.props.updateFormData({
      [field]: field === 'quantity' ? parseInt(value, 10) : value
    });
  }

  initialiseData = () => {
    const initialQuantity = this.props.item.quantity;
    const staticDetails = this.constructor.getStaticProperties(this.props.item);

    this.props.resetFormData({});
    this.props.updateFormData({
      ...staticDetails,
      quantity: initialQuantity
    });
  }

  updatePayload = (item, data) => {
    const staticProperties = this.constructor.getStaticProperties(item);
    const orderDetails = this.constructor.getOrderDetails(data);
    const remainingQuantity = orderDetails.quantity
      ? (item.quantity - orderDetails.quantity)
      : 0;

    return {
      item: {
        ...item,
        quantity: remainingQuantity
      },
      order: {
        quantity: item.quantity,
        ...staticProperties,
        ...orderDetails
      }
    };
  }

  submitOrder = () => {
    this.props.orderItem(this.payload);
  }

  render = () => {
    const { item, data } = this.props;
    const { material } = item;
    const { quantity } = data;
    const validated = this.constructor.validate(data);

    this.payload = this.updatePayload(item, data);

    return (
      <div>
        <div className="static-details">
          {
            ['steel', 'brick', 'timber'].includes(material) ?
              <div>Type: {item.type}</div> :
              ''
          }
          {
            material === 'rebar' ?
              <div>Diameter: {item.diameter}</div> :
              ''
          }
          {
            material === 'block' ?
              <div>Density: {item.density}</div> :
              ''
          }
          {
            ['brick', 'block'].includes(material) ?
              <div>Strength: {item.strength}</div> :
              ''
          }
          {
            material === 'steel' ?
              <div>Section: {item.section}</div> :
              ''
          }
          {
            ['steel', 'timber'].includes(material) ?
              <div>Grade: {item.grade}</div> :
              ''
          }
          {
            material === 'block' ?
              <div>Size: {item.blocksize}</div> :
              ''
          }
          {
            material === 'block' ?
              <div>Finish: {item.finish}</div> :
              ''
          }
          {
            ['steel', 'timber', 'rebar'].includes(material) ?
              <div>Length: {item.length}</div> :
              ''
          }
        </div>
        <Input
          field="quantity"
          value={quantity || ''}
          id="quantity"
          type="number"
          placeholder="Quantity"
          handleChange={this.onUpdate}
        />
        <Select
          selected={data.job_number || ''}
          field="job_number"
          id="job_number"
          label="Job No."
          options={['1900', '0076']}
          handleChange={this.onUpdate}
        />
        <Input
          field="project"
          id="project"
          value={data.project || ''}
          type="text"
          placeholder="Project Name"
          handleChange={this.onUpdate}
        />
        <DatePicker
          handleChange={this.onUpdate}
        />
        <Button
          dispatch={this.submitOrder}
          label="Order Item"
          id="order-item"
          theme="light"
          loading={this.props.loading}
          disabled={validated}
        />
      </div>
    );
  }
}

export default OrderItem;
