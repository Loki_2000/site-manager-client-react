import { LOAD_ITEMS, LOAD_ITEM, SELECT_ITEM } from './actions';


export const itemsReducer = (state = {}, { type, payload }) => {
  switch (type) {
  case LOAD_ITEMS: {
    const { items } = payload;

    return { ...items };
  }
  case LOAD_ITEM: {
    const item = {};
    item[payload._id] = payload;

    return {
      ...state,
      ...item
    };
  }
  default: {
    return state;
  }
  }
};

export const itemIDsReducer = (state = [], { type, payload }) => {
  switch (type) {
  case LOAD_ITEMS: {
    const { itemIDs } = payload;

    return [...itemIDs];
  }
  case LOAD_ITEM: {
    const itemID = payload._id;

    return [...state, itemID];
  }
  default: {
    return state;
  }
  }
};

export const selectedItemID = (state = '', { type, id }) => {
  switch (type) {
  case SELECT_ITEM: {
    return id;
  }
  default: {
    return state;
  }
  }
};
