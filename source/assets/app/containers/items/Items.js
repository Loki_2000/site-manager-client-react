import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';

import { getItems, selectItem } from '../../modules/items/actions';

import Items from '../../components/items';

const mapDispatchToProps = dispatch => (bindActionCreators({
  getItems,
  selectItem
}, dispatch));

const mapStateToProps = state => ({
  items: state.items,
  itemIDs: state.itemIDs
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Items));
