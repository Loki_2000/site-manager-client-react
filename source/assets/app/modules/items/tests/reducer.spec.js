import {
  itemsReducer,
  itemIDsReducer,
  selectedItemID as selectedItemIDReducer
} from '../reducer';

describe('Item Reducers', () => {
  const itemID = '0987';
  const payload = {
    items: {
      [itemID]: {
        _id: '0987',
        type: 'Equal Angles',
        section: '200x200'
      }
    },
    itemIDs: [itemID]
  };

  describe('LOAD_ITEMS: items', () => {
    const initialState = Object.freeze({});
    it('should refresh state with items', () => {
      const state = itemsReducer(initialState, {
        type: 'items/LOAD_ITEMS',
        payload
      });

      expect(state).toEqual({ ...initialState, ...payload.items });
    });
  });

  describe('LOAD_ITEM: items', () => {
    const item = payload.items[itemID];
    const initialState = Object.freeze({
      '0365': {
        _id: '0987',
        type: 'Equal Angles',
        section: '200x200'
      }
    });

    it('should update state with new items', () => {
      const state = itemsReducer(initialState, {
        type: 'items/LOAD_ITEM',
        payload: item
      });

      expect(state).toEqual({ ...initialState, [itemID]: item });
    });
  });

  describe('LOAD_ITEMS: itemIDs', () => {
    const initialState = Object.freeze(['0365']);

    it('should refresh state with itemIDs', () => {
      const state = itemIDsReducer(initialState, {
        type: 'items/LOAD_ITEMS',
        payload
      });

      expect(state).toEqual([...payload.itemIDs]);
    });
  });

  describe('LOAD_ITEM: itemIDs', () => {
    const initialState = Object.freeze(['0365']);
    const item = payload.items[itemID];

    it('should update state with new itemID', () => {
      const state = itemIDsReducer(initialState, {
        type: 'items/LOAD_ITEM',
        payload: item
      });

      expect(state).toEqual([...initialState, item._id]);
    });
  });

  describe('selectedItemID', () => {
    const initialState = '';

    it('should update state with selectedItemID', () => {
      const state = selectedItemIDReducer(initialState, {
        type: 'items/SELECT_ITEM',
        id: itemID
      });

      expect(state).toEqual(itemID);
    });
  });
});
