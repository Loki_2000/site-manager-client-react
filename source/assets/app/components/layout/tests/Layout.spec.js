import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Layout from '../';

describe('Layout Component', () => {
  let component;
  let tree;

  const props = {
    getTypes: jest.fn(),
    layout: {
      modalOpen: false,
      popUpOpen: false,
      section: ''
    },
    updateLayout: jest.fn(),
    history: {
      listen: jest.fn()
    }
  };

  beforeEach(() => {
    component = shallow(<Layout {...props} />);
    tree = toJson(component);
  });

  describe('Component Exists', () => {
    it('Should render the Layout Component', () => {
      expect(component.length).toEqual(1);
    });

    it('Should match previous snapshot', () => {
      expect(tree).toMatchSnapshot();
    });
  });

  describe('Open PopUp', () => {
    it('should call props.updateLayout with expected params', () => {
      props.layout.popUpOpen = true;
      const popUpOpen = !props.layout.popUpOpen;
      const menu = 'add-item';
      const expected = { popUpOpen, section: menu };

      component.instance().openPopUp(menu);
      expect(props.updateLayout).toHaveBeenCalledWith(expected);
    });

    it('Should match previous snapshot', () => {
      expect(tree).toMatchSnapshot();
    });
  });
});
