import React from 'react';
import PropTypes from 'prop-types';

import './input.scss';

const Input = ({
  value, id, handleChange, type, placeholder, display, theme
}) => {
  if (!display) {
    return ('');
  }
  return (
    <div className="input">
      <input
        theme={theme}
        value={value}
        id={id}
        onChange={(e => handleChange(e.target.id, e.target.value))}
        type={type}
        placeholder={placeholder}
      />
    </div>
  );
};
export default Input;

Input.propTypes = {
  handleChange: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]).isRequired,
  display: PropTypes.bool,
  theme: PropTypes.string
};

Input.defaultProps = {
  display: true,
  theme: 'dark'
};
