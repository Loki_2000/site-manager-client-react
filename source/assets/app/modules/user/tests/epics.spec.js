import fetchMock from 'fetch-mock';
import configureMockStore from 'redux-mock-store';
import { createEpicMiddleware, combineEpics } from 'redux-observable';
import { sendLoginRequest, checkUserLogin } from '../epics';
import LocalStorageMock from '../../../../tests';
import { setTimeout } from 'timers';

const epics = combineEpics(sendLoginRequest, checkUserLogin);
const epicMiddleware = createEpicMiddleware(epics);
const mockStore = configureMockStore([epicMiddleware]);


describe('User Epic', () => {
  let store;
  global.localStorage = new LocalStorageMock();

  const req = {
    username: 'test@test.com',
    password: '1234'
  };

  const res = {
    _id: '098765',
    id: '098765',
    username: 'test@test.com',
    firstname: 'Test',
    lastname: 'Test',
    phone: '09876543',
    isAdmin: false
  };

  beforeEach(() => {
    store = mockStore();
  });

  afterEach(() => {
    fetchMock.restore();
    epicMiddleware.replaceEpic(sendLoginRequest);
  });

  describe('sendLoginReequest', () => {
    it('should make an api login request and return expected action', (done) => {

      fetchMock.post('/api/security', { user: res });

      store.dispatch({
        type: 'user/REQUEST_LOGIN',
        payload: req
      });

      setTimeout(() => {
        expect(store.getActions()).toEqual([
          {
            type: 'user/REQUEST_LOGIN',
            payload: req
          }, {
            type: 'layout/UPDATE_LAYOUT',
            payload: {
              layout: {
                loading: true
              }
            }
          }, {
            type: 'user/USER_LOGGED_IN',
            payload: { user: res }
          }, {
            type: 'layout/UPDATE_LAYOUT',
            payload: {
              layout: {
                loading: false
              }
            }
          }, {
            type: 'formData/RESET_FORM_DATA',
            payload: {}
          }
        ]);
        done();
      });
    });
  });

  describe('checkUserLogin', () => {
    it('should checkLogged in details and return the expected action', (done) => {
      const { id } = res;
      const expected = [{
        type: 'user/CHECK_LOGIN',
        id: '098765'
      }, {
        type: 'user/USER_LOGGED_IN',
        payload: { user: res }
      }];

      fetchMock.get('/api/security/098765', { user: res });

      store.dispatch({
        type: 'user/CHECK_LOGIN',
        id
      });

      setTimeout(() => {
        expect(store.getActions()).toEqual(expected);
        done();
      });
    });
  });
});
