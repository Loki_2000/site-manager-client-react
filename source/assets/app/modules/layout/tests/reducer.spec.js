import layoutReducer from '../reducer';

describe('Layout Reducers', () => {
  const payload = {
    modalOpen: true
  };

  const layout = {
    modalOpen: false,
    section: ''
  };

  describe('layout/UPDATE_LAYOUT', () => {
    const initialState = Object.freeze({ ...layout });

    it('should update state with expected payload', () => {
      const state = layoutReducer(initialState, {
        type: 'layout/UPDATE_LAYOUT',
        payload: { layout: payload }
      });

      expect(state).toEqual({ ...initialState, ...payload });
    });
  });
});
