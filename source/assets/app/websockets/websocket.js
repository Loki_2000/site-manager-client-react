import Socket from 'socket.io-client';

export const setupWebsockets = (userId, dispatch) => {

	const socket = new Socket('ws://localhost:8080', {
		port: 8080
	});

	const sendMessage = (message) => {
		socket.emit('message', message);
	}

	socket.connect();

	socket.on('connect', () => {
		console.log('Client hs connected to the server')
		const msg = 
		sendMessage({type: 'login', data: {id: userId}});
	});

	socket.on('message', (msg) => {
		switch (msg.type) {
			case 'notification':
			dispatch([]);
			alert(msg.data.title);

			break;
		}
	});

	socket.on('disconnect', () => {
		console.log('Client has disconnected!');
		sendMessage('logout', {id: userId});
	});
}