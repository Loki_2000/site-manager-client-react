import _ from 'lodash';
import { UPDATE_FORM_DATA, RESET_FORM_DATA, SET_DEFAULT_DATA } from './actions';
import * as formDefaults from './formDefaults';

const FormData = {};

export default (state = FormData, { type, payload }) => {
  switch (type) {
  case UPDATE_FORM_DATA: {
    const { formData } = payload;

    return {
      ...state,
      ...formData
    };
  }
  case RESET_FORM_DATA: {
    const defaultData = payload;

    return {
      ...defaultData,
      ...FormData
    };
  }
  case SET_DEFAULT_DATA: {
    const defaultData = formDefaults[payload];

    return {
      ...defaultData
    };
  }
  default: {
    return state;
  }
  }
};
