import { updateLayout } from '../actions';

describe('Layout Actions', () => {
  const layout = {
    modalOpen: true
  };

  describe('updateLayout', () => {
    it('should call updateLayout with expected action', () => {
      const expected = {
        type: 'layout/UPDATE_LAYOUT',
        payload: { layout }
      };
      const action = updateLayout(layout);
      expect(action).toEqual(expected);
    });
  });
});
