import React from 'react';
import PropTypes from 'prop-types';
import './Select.scss';

const Select = ({
  options, field, handleChange, label, display, selected, theme, size, defaultPlaceholder
}) => {
  if (!display) {
    return ('');
  }

  return (
    <div className="select">
      <div className="select-icon" />
      <select
        className={!selected ? `placeholder ${field} ${size}` : `${field} ${size}`}
        theme={theme}
        value={selected}
        onChange={(e => handleChange(field, e.target.value))}
      >
        <option value="">{defaultPlaceholder} {label}</option>
        {options.map(option => (
          <option
            key={`${field}_${option}`}
            value={option}
          >
            {option}
          </option>))
        }
      </select>
    </div>
  );
};

export default Select;

Select.propTypes = {
  defaultPlaceholder: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.string).isRequired,
  handleChange: PropTypes.func.isRequired,
  theme: PropTypes.string,
  field: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  display: PropTypes.bool,
  selected: PropTypes.string,
  size: PropTypes.string
};

Select.defaultProps = {
  defaultPlaceholder: 'Select',
  display: true,
  selected: '',
  theme: 'dark',
  size: 'lg'
};
