/* eslint-env node, es6 */

const gutil = require('gulp-util'); // Helpful library for logging task details

const autoprefixer = require('autoprefixer');
const connect = require('gulp-connect');
const cssmin = require('gulp-cssmin');
const gulp = require('gulp');
const gulpFilter = require('gulp-filter');
const htmlmin = require('gulp-htmlmin');
const notifier = require('node-notifier');
const notify = require('gulp-notify');
const path = require('path');
const postcss = require('gulp-postcss');
const preprocess = require('gulp-preprocess');
const proxy = require('http-proxy-middleware');
const rename = require('gulp-rename');
const sass = require('gulp-sass');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackStream = require('webpack-stream');
const webpack = require('webpack');

const src = './source';
const dest = './build';

// Config file to be used for frequently changed properties
const userConfig = require('./config.defaults');

// Centralise various things we will repeatedly use
const sassConfig = {
  npmDir: './node_modules',
  path: './build/assets/stylesheets'
};

const paths = {
  html: path.join(src, '**/*.html'),
  other: path.join(src, '**/{*.jpg,*.png,*.svg,*.txt,*.ico,*.otf,*.woff,*.woff2,*.xml,*.gif,*.webmanifest}'),
  sass: path.join(src, '**/assets/stylesheets/**/*.scss')
};

const webpackConfig = require('./webpack.config.js');
const compiler = webpack(require('./webpack.config.js'));

// Setup webpack
gulp.task('webpack', () => gulp.src('source/assets/app.js')
  .pipe(webpackStream(webpackConfig))
  .pipe(gulp.dest('build/assets/'))
  .pipe(gulpFilter(['**/*.js'])));

// Compile and minify sass
gulp.task('css', () => gulp.src(paths.sass)
  .pipe(sass({
    includePaths: [
      './source/assets/stylesheets',
      sassConfig.npmDir,
      `${sassConfig.npmDir}/bootstrap-sass/assets/stylesheets`
    ],
    outputStyle: 'compressed'
  }).on('error', notify.onError(error => `Error: ${error.message}`)))
  .pipe(rename({ dirname: 'assets/stylesheets' }))
  .pipe(postcss([
    autoprefixer({
      // NOTE: How to specify browser targets: https://github.com/ai/browserslist
      browsers: ['last 2 Chrome versions', 'last 2 Firefox versions', 'last 2 edge versions', 'ie > 9', 'Safari >= 8']
    })
  ]))
  .pipe(cssmin())
  .pipe(rename({ suffix: '.min' }))
  .pipe(gulp.dest(dest))
  .pipe(userConfig.livereload ? connect.reload() : gutil.noop()));


// Fonts
gulp.task('fonts', () => gulp.src([
  `${sassConfig.npmDir}/font-awesome/fonts/fontawesome-webfont.*`])
  .pipe(gulp.dest(`${dest}/assets/fonts`)));


// Watch for changes in assets files
gulp.task('watch', ['build'], () => {
  gulp.watch(paths.sass, ['css']);

  gulp.watch(paths.html, ['html']);

  gulp.watch(paths.other, ['copy']);
});

// Start development server
gulp.task('connect', () => {
  connect.server({
    fallback: './build/index.html',
    livereload: true,
    middleware() {
      const devPack = webpackDevMiddleware(compiler, {
        headers: {
          'X-webpack-dev': 'true'
        },
        publicPath: '/assets/',
        stats: {
          chunks: false,
          colors: true,
          timings: true
        }
      });

      compiler.plugin('done', () => {
        gutil.log('Webpack incremental compile, triggering livereload');
        gulp.src(path.join(dest, '**/*.html')).pipe(connect.reload());
      });

      const endpoints = [
        '/api'
      ];

      const p = proxy(endpoints, {
        target: userConfig.apiHost,
        ws: true
      });
      return [devPack, p];
    },
    port: userConfig.port || 8000,
    root: ['build', 'source']
  });
});

gulp.task('html', () => {
  const minifier = htmlmin({
    collapseWhitespace: true,
    ignoreCustomComments: [/^(v|\[)/],
    minifyJS: true,
    removeComments: true
  });

  gulp.src(paths.html)
    .pipe(preprocess())
    .pipe(minifier)
    .pipe(gulp.dest(dest))
    .pipe(userConfig.livereload ? connect.reload() : gutil.noop());
});


//
gulp.task('copy', () => gulp.src(paths.other, { base: path.resolve(src) })
  .pipe(gulp.dest(dest))
  .pipe(userConfig.livereload ? connect.reload() : gutil.noop()));

/*
 *  Does a build, starts the development server and watches for changes
 */
gulp.task('develop', ['connect', 'watch'], () => {
  const msg = `Server listening on http://localhost:${userConfig.port}/ `;
  gutil.log(gutil.colors.bgGreen.bold.white(`  ${msg}`));

  notifier.notify({
    message: msg,
    sound: true,
    title: 'Gulp is Ready!'
  });
});

/*
 *  Calls all the build tasks.
 */
gulp.task('build', ['css', 'html', 'fonts', 'webpack'], () => {
  gutil.log('Creating Build');
});
