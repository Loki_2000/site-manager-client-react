import userReducer from '../reducer';

describe('Types Reducers', () => {
  const user = {
    id: null,
    username: '',
    firstname: '',
    lastname: '',
    phone: '',
    isAdmin: false,
    isLoggedIn: false
  };

  const loggedInUser = {
    id: '098765',
    username: 'test@test.com',
    firstname: 'Test',
    lastname: 'Test',
    phone: '09876543',
    isAdmin: false
  };

  describe('USER_LOGGED_IN', () => {
    const initialState = Object.freeze({ ...user });
    it('should refresh state with expected types', () => {
      const state = userReducer(initialState, {
        type: 'user/USER_LOGGED_IN',
        payload: {
          user: loggedInUser
        }
      });

      expect(state).toEqual({
        ...initialState,
        ...loggedInUser,
        isLoggedIn: true
      });
    });
  });
});
