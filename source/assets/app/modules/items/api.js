import { normalize } from 'normalizr';

import { itemSchema } from '../../schemas';

export const getItems = () => {
  const url = '/api/items';

  const OPTIONS = {
    credentials: 'same-origin',
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
  };

  return fetch(url, OPTIONS)
    .then((response) => {
      if (!response.ok) {
        return Promise.reject(response);
      }
      return Promise.resolve(response.json());
    })
    .then((json) => {
      const data = { items: json };
      const { entities, result } = normalize(data, itemSchema);

      return {
        items: entities.items,
        itemIDs: result.items
      };
    });
};

export const addItem = (payload) => {
  const url = '/api/items/add';

  const OPTIONS = {
    credentials: 'same-origin',
    method: 'POST',
    body: JSON.stringify(payload),
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
  };

  return fetch(url, OPTIONS)
    .then((response) => {
      if (!response.ok) {
        return Promise.reject(response);
      }
      return Promise.resolve(response.json());
    })
    .then(json => json);
};

export const createOrder = (payload) => {
  const url = '/api/orders/add';

  const OPTIONS = {
    credentials: 'same-origin',
    method: 'POST',
    body: JSON.stringify(payload),
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
  };

  return fetch(url, OPTIONS)
    .then((response) => {
      if (!response.ok) {
        return Promise.reject(response);
      }
      return Promise.resolve(response.json());
    })
    .then(json => json);
};

export const updateItem = (payload) => {
  const id = payload._id;
  const url = `/api/items/update/${id}`;

  const OPTIONS = {
    credentials: 'same-origin',
    method: 'PUT',
    body: JSON.stringify(payload),
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
  };

  return fetch(url, OPTIONS)
    .then((response) => {
      if (!response.ok) {
        return Promise.reject(response);
      }
      return Promise.resolve(response.json());
    })
    .then(json => json);
};
