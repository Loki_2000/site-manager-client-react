import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import AddItem from '../';

describe('Items Component', () => {
  let component;
  let tree;

  const typeIDs = ['steel', 'timber', 'brick', 'block', 'rebar'];
  const props = {
    filters: {
      material: '',
      section: '',
      type: ''
    },
    types: {
      [typeIDs[0]]: {
        material: 'steel',
        type: 'UC',
        sections: ['356x406x634'],
        types: ['UB']
      },
      [typeIDs[1]]: {
        material: 'timber',
        types: ['5mm PLY', '18mm PLY']
      },
      [typeIDs[2]]: {
        material: 'brick',
        types: ['Flettons', 'Glazed']
      },
      [typeIDs[3]]: {
        material: 'block',
        densities: ['Heavy', 'Semi']
      },
      [typeIDs[4]]: {
        material: 'rebar',
        diameters: ['H8', 'H12']
      }
    },
    data: {
      type: '',
      section: '',
      grade: '',
      length: 3,
      quantity: 0,
      job_number: 1234,
      project: '',
      diameter: '',
      strength: '',
      blocksize: '',
      density: '',
      material: '',
      finish: ''
    },
    updateFormData: jest.fn(),
    updateLayout: jest.fn()
  };

  beforeEach(() => {
    component = shallow(<AddItem {...props} />);
    tree = toJson(component);
  });

  describe('Component Exists', () => {
    it('Should render the Items Component', () => {
      expect(component.length).toEqual(1);
    });
  });

  describe('onUpdate', () => {
    it('should call updateFormData and updateFilters  with expected', () => {
      let field = 'type';
      let value = 'UB';
      let expected = { [field]: value };
      let filters = {
        ...props.filters,
        ...expected
      };

      component.instance().onUpdate(field, value);

      expect(props.updateFormData).toHaveBeenCalledWith(expected);
      expect(props.updateLayout).toHaveBeenCalledWith({ filters });

      field = 'length';
      value = '2';
      expected = { [field]: 2 };

      filters = {
        ...props.filters,
        ...expected
      };

      component.instance().onUpdate(field, value);
      expect(props.updateFormData).toHaveBeenCalledWith(expected);
      expect(props.updateLayout).toHaveBeenCalledWith({ filters });
    });
  });

  describe('parseOptions', () => {
    it('should loop through material types and return expected array of options', (done) => {
      let expected;
      let parseOptions;

      typeIDs.forEach((material) => {
        props.filters.material = material;
        props.filters.type = 'UC';

        Object.keys(props.types[material])
          .filter(key => Array.isArray(props.types[material][key]))
          .forEach((key) => {
            expected = props.types[material][key];
            parseOptions = component.instance().parseOptions(key);

            expect(parseOptions).toEqual(expected);
          });
      });
      // Reseting changed props
      props.filters.material = '';
      props.data.type = '';
      done();
    });
  });

  describe('Snapshot Match', () => {
    it('Should match previous snapshot', () => {
      expect(tree).toMatchSnapshot();
    });
  });
});
