import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';

import { checkLogin } from '../../modules/user/actions';
import Application from '../../components/application';

const mapDispatchToProps = dispatch => (bindActionCreators({
  checkLogin
}, dispatch));

const mapStateToProps = state => ({
  user: state.user
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Application));
