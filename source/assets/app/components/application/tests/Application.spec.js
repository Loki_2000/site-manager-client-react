import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Application from '../';
import LocalStorageMock from '../../../../tests';

describe('Application Component', () => {
  global.localStorage = new LocalStorageMock();

  let user;
  let component;
  let tree;

  const checkLogin = jest.fn();

  const resetComponent = (newUser) => {
    component = shallow(<Application
      user={newUser}
      checkLogin={checkLogin}
    />);
    tree = toJson(component);
  };

  beforeEach(() => {
    user = {
      id: 'p23o',
      username: 'test@t.com',
      isLoggedIn: false
    };

    localStorage.setItem('userId', user.id);
    localStorage.setItem('token', 'TEST');

    component = shallow(<Application
      user={user}
      checkLogin={checkLogin}
    />);
    tree = toJson(component);
  });

  describe('Component Exists', () => {
    it('Should render the Items Component', () => {
      expect(component.length).toEqual(1);
    });
  });

  describe('isLoggedIn', () => {
    it('should call checkLogin when token and id are stored', () => {
      component.instance().isLoggedIn();

      expect(checkLogin).toHaveBeenCalledWith(user.id);
      expect(tree).toMatchSnapshot();
    });

    it('should not call checkLogin if user isLoggedIn', () => {
      resetComponent({ ...user, isLoggedIn: true });

      component.instance().isLoggedIn();

      expect(checkLogin).not.toHaveBeenCalledWith();
      expect(tree).toMatchSnapshot();
    });
  });
});
