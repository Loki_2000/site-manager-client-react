import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { updateFormData } from '../../../modules/formData/actions';
import { requestLogin } from '../../../modules/user/actions';
import { Login } from '../../../components/forms';


const mapDispatchToProps = dispatch => (bindActionCreators({
  submit: requestLogin,
  update: updateFormData
}, dispatch));

const mapStateToProps = state => ({
  data: state.formData,
  loading: state.layout.loading
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
