import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Button from '../button';
import Form from '../form';

import './Modal.scss';

class Modal extends Component {
  static propTypes = {
    content: PropTypes.string.isRequired
  }

  constructor(props) {
    super(props);
    props.setLayout({ state: this.props.match.params });
  }

  closeModal = () => {
    const section = this.props.match.params;
    this.props.history.push({
      pathname: `/items/${section.material}`,
      state: section
    });
  }

  switchContent() {
    const { params } = this.props.match;

    switch (this.props.content) {
    case 'add-item':
      return (
        <Form material={params.material} type="add-item-form" />
      );

    case 'order-item':
      return (
        <Form type="order-item-form" />
      );

    case 'filter-items':
      return (
        <Form type="filter-items-form" />
      );

    default:
      return ('');
    }
  }

  render() {
    return (
      <div className="modal-container">
        <div className="modal-head">
          <div className="item-symbol">
            <img src={`/assets/images/${this.props.theme}.png`} alt={this.props.theme} />
          </div>
          <div className="close-modal">
            <Button
              id="close-modal"
              type="icon"
              icon="fa fa-times"
              theme="light"
              dispatch={this.closeModal}
            />
          </div>
        </div>
        { this.switchContent() }
      </div>
    );
  }
}

export default Modal;
