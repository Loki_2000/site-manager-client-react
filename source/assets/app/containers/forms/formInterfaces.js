export const brickInterface = [
  'material',
  'type',
  'strength',
  'quantity',
  'job_number',
  'project'
];

export const blockInterface = [
  'material',
  'density',
  'strength',
  'blocksize',
  'finish',
  'quantity',
  'job_number',
  'project'
];

export const rebarInterface = [
  'material',
  'diameter',
  'length',
  'quantity',
  'job_number',
  'project'
];

export const steelInterface = [
  'material',
  'type',
  'section',
  'grade',
  'length',
  'quantity',
  'job_number',
  'project'
];

export const timberInterface = [
  'material',
  'type',
  'grade',
  'length',
  'quantity',
  'job_number',
  'project'
];

export const loginInterface = [
  'username',
  'password'
];
