import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { setDefaultData, updateFormData, resetFormData } from '../../../modules/formData/actions';
import { addItem } from '../../../modules/items/actions';
import { AddItem } from '../../../components/forms';

const mapDispatchToProps = dispatch => (bindActionCreators({
  setDefaultData,
  updateFormData,
  resetFormData,
  addItem
}, dispatch));

const mapStateToProps = state => ({
  data: state.formData,
  loading: state.layout.loading,
  types: state.types
});

export default connect(mapStateToProps, mapDispatchToProps)(AddItem);
