import React, { Component } from 'react';
import { PropTypes } from 'prop-types';

import FilterItems from '../../containers/forms/filterItems';
import Item from '../item';
import './Items.scss';

class Items extends Component {
  constructor(props) {
    super(props);

    props.getItems();
    props.setLayout({
      state: this.props.match.params
    });
  }

  onSelectItem = (id, material) => {
    this.props.selectItem(id);
    this.props.history.push({
      pathname: `/items/${material}/order`,
      state: { material }
    });
  }

  filterItems = (id) => {
    let filter = true;
    const { items, filters } = this.props;

    if (!Object.values(filters)
      .filter(Boolean).length) {
      return true;
    }

    Object.keys(filters)
      .filter(key => !!filters[key])
      .forEach((key) => {
        if (filters[key] !== items[id][key]) {
          filter = false;
        }
      });

    return filter;
  }

  showFilter = () => (
    <FilterItems history={this.props.history} />
  );

  render = () => {
    const { items, itemIDs } = this.props;

    return (
      <div className="items">
        {this.showFilter()}
        {
          itemIDs.filter(id => this.filterItems(id))
            .map(id => (
              <Item
                selectItem={this.onSelectItem}
                key={id}
                id={id}
                item={items[id]}
              />
            ))
        }
      </div>
    );
  }
}

export default Items;

Items.propTypes = {
  filters: PropTypes.objectOf(PropTypes.string).isRequired,
  getItems: PropTypes.func.isRequired,

  history: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.object,
    PropTypes.func
  ])).isRequired,

  match: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.object,
    PropTypes.bool,
    PropTypes.func
  ])).isRequired,

  items: PropTypes.objectOf(PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.bool
  ]))).isRequired,

  itemIDs: PropTypes.arrayOf(PropTypes.string).isRequired,
  selectItem: PropTypes.func.isRequired,
  setLayout: PropTypes.func.isRequired
};
